package test.youtubestreamerproxy

import android.app.PictureInPictureParams
import android.content.res.Configuration
import android.media.AudioManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.media.MediaDescriptionCompat
import android.support.v4.media.session.MediaSessionCompat
import android.util.Rational
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector
import com.google.android.exoplayer2.ext.mediasession.TimelineQueueNavigator
import kotlinx.android.synthetic.main.activity_main.*
import test.youtubestreamerproxy.player.PlayerHolder
import test.youtubestreamerproxy.player.PlayerState

class MainActivity : AppCompatActivity() {

    private val playList: List<MediaDescriptionCompat> = listOf(
        with(MediaDescriptionCompat.Builder()) {
            setMediaUri(Uri.parse("http://10.0.2.2:8405/"))
            setMediaId("1")
            setTitle("Quantic - Time Is The Enemy")
            setSubtitle("Streaming video")
            setDescription("...")
            build()
        }
    )

    private val mediaSession: MediaSessionCompat by lazy { MediaSessionCompat(this, packageName) }
    private val mediaSessionConnector: MediaSessionConnector by lazy {
        MediaSessionConnector(mediaSession).apply {
            // If QueueNavigator isn't set, then mediaSessionConnector will not handle following
            // MediaSession actions (and they won't show up in the minimized PIP activity):
            // [ACTION_SKIP_PREVIOUS], [ACTION_SKIP_NEXT], [ACTION_SKIP_TO_QUEUE_ITEM]
            setQueueNavigator(object : TimelineQueueNavigator(mediaSession) {
                override fun getMediaDescription(windowIndex: Int): MediaDescriptionCompat {
                    return playList[windowIndex]
                }
            })
        }
    }
    private val playerState by lazy { PlayerState() }
    private lateinit var playerHolder: PlayerHolder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
        }

        // While the user is in the app, the volume controls should adjust the music volume.
        volumeControlStream = AudioManager.STREAM_MUSIC

        // Create ExoPlayer
        playerHolder = PlayerHolder(
            this,
            playerState,
            exoPlayerView,
            playList
        )
    }

    override fun onStart() {
        super.onStart()
        playerHolder.start()
        activateMediaSession()
    }

    override fun onStop() {
        super.onStop()
        playerHolder.stop()
        deactivateMediaSession()
    }

    override fun onDestroy() {
        super.onDestroy()
        playerHolder.release()
        releaseMediaSession()
    }

    private fun activateMediaSession() {
        mediaSessionConnector.setPlayer(playerHolder.audioFocusPlayer, null)
        mediaSession.isActive = true
    }

    private fun deactivateMediaSession() {
        mediaSessionConnector.setPlayer(null, null)
        mediaSession.isActive = false
    }

    private fun releaseMediaSession() {
        mediaSession.release()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    // Picture in Picture related functions.
    override fun onUserLeaveHint() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enterPictureInPictureMode(
                with(PictureInPictureParams.Builder()) {
                    val width = 16
                    val height = 9
                    setAspectRatio(Rational(width, height))
                    build()
                })
        }
    }

    override fun onPictureInPictureModeChanged(
        isInPictureInPictureMode: Boolean,
        newConfig: Configuration?
    ) {
        appBar.visibility = if (isInPictureInPictureMode) View.GONE else View.VISIBLE
        exoPlayerView.useController = !isInPictureInPictureMode
    }
}