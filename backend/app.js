
let http = require('http');
let streamer = require('./streamer.js');
let cluster = require('cluster');

const IP = '127.0.0.1';
const PORT = Number(process.env.PORT || 8405);

// If this is the master process, fork child process
if(cluster.isMaster){
    cluster.fork();
    
    // Check for the child's states and restart them if died
    cluster.on('exit', function(worker, code) {
        console.log('Worker ' + worker.process.pid + ' died with code '+code);
        cluster.fork();
    });
    cluster.on('online', function(worker) {
        console.log('Worker ' + worker.process.pid + ' is ready!');
    });
}
else{
    // We're in the child process. Start the HTTP server.
    let server = http.createServer(streamer());
    
    // Listen on the specified IP and port.
    server.listen(PORT,null,function(){
        console.log('Server #%d listening at %s:%d',process.pid,'*',PORT);
    });
}