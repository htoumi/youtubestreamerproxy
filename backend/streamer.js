// Streams YouTube video to clients using a proxy-like implantation.

let ytdl = require('ytdl-core');
let fs = require('fs');

const YOUTUBE_VIDEO_ID = 'nvUeo5sagkA'
const CACHE_DIR = './cache'
const VIDEO_PATH = CACHE_DIR + '/video.mp4'

let playVideo = function(req, res){
	let stat = fs.statSync(VIDEO_PATH);
	let fileSize = stat.size;
	let range = req.headers.range;

	if (range) {
		let parts = range.replace(/bytes=/, "").split("-");
		let start = parseInt(parts[0], 10);
		let end = parts[1] ? parseInt(parts[1], 10) : fileSize-1;

		let chunksize = (end-start)+1;
		let file = fs.createReadStream(VIDEO_PATH, {start, end});
		let head = {
			'Content-Range': `bytes ${start}-${end}/${fileSize}`,
			'Accept-Ranges': 'bytes',
			'Content-Length': chunksize,
			'Content-Type': 'video/mp4',
		}
		res.writeHead(206, head);
		file.pipe(res);
	} else {
		let head = {
			'Content-Length': fileSize,
			'Content-Type': 'video/mp4',
		}
		res.writeHead(200, head);
		fs.createReadStream(VIDEO_PATH).pipe(res);
	}
}

module.exports = function(videoId){
	
	return function(req, res){

		if (fs.existsSync(VIDEO_PATH)) {
			console.log('Youtube video already fetched');

			playVideo(req, res);
		} else {
			console.log('fetching Youtube video...');

			if (!fs.existsSync(CACHE_DIR)){
    			fs.mkdirSync(CACHE_DIR);
			}

			ytdl('http://www.youtube.com/watch?v=' + YOUTUBE_VIDEO_ID, { filter: format => format.container === 'mp4' })
				.pipe(fs.createWriteStream(VIDEO_PATH)
						.on('close', function() {
							console.log('Done fetching video');
							playVideo(req, res);
						})
				);
		}
	};
};