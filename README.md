YoutubeStreamer Proxy
====================

Streaming Youtube's videos using a proxy like implementation.

This project is composed of:
- Node.js streaming server
- Android video player client app

#### Setup Node.js server

Just execute this on your console:

	$ cd ./backend
	$ node app.js

After launching the server, on your browser you can test the video streaming using this URL http://127.0.0.1:8405/

#### Android client app

- Launch the streaming server
- Build the android project and run it on your emulator (it uses this URL to stream the video http://10.0.2.2:8405/)
- That's it, the video is playing !

The Android app uses ExoPlayer with MediaSession and Picture-in-Picture support